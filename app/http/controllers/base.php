<?php namespace controllers;

use controllers\auth\Auth;
use providers\request\Request;
use providers\view\Views;

abstract class base {
    public const AUTO_RUN_FUNC_BYMETHOD = [
        'POST' => 'save',
        'GET' => 'get',
        'UPDATE' => 'update',
        'DELETE' => 'delete'
    ];
    public function run_func_bymethod()
    {
        $params = func_get_args();
        $func = self::AUTO_RUN_FUNC_BYMETHOD[strtoupper($_SERVER['REQUEST_METHOD'])];
        $this->view = new Views;
        $this->globalVals = [];
        $this->auth = new Auth($this->view);
        $this->auth->APPNAME = "Evolt";
        
        return call_user_func_array([$this, $func], $params);
    }
    public function render($name, $data)
    {
        return $this->view->render($name, array($data, $this->globalVals));
    }
}