<?php namespace controllers;

use app\database\user as DatabaseUser;
use app\database\usermeta;
use providers\request\Request as RequestRequest;
use providers\view\Views;

class user extends base {
    function save()
    {
        $request = RequestRequest::getInstants()->getAll();
        // add new user with userpassword is set.
        if (isset($request['password'])) {
            $user = new DatabaseUser;
            $request['user_id'] = $user->insert([
                'name' => $request['name'],
                'password' => md5($request['password'])
            ]);
        }

        // add new user meta if viewstyle is set.
        if (isset($request['viewstyle'])) {
            $this->add_userMeta($request);
        }

        $allUser = $this->get_all_user($request);
        // get viewtype from database
        $usermt = new usermeta;
        $viewtype = $usermt
        ->where('user_id', '=', 1)
        ->get();

        return Views::ResponseJson(
            'component.'.$viewtype['viewstyle'],
            [
                'head' => ['ชื่อ', 'วันที่สร้าง', 'ลบ'],
                'recs' => $allUser
            ]
        );
    }
    public function get()
    {
        return $this->render(
            'home', []
        );
    }
    public function update()
    {
        $update = RequestRequest::getInstants()->getAll();

        $user 
        = new DatabaseUser;
        $user->update([
            $update['column'] => $update['value']
        ])->where('id', '=', $update['id'])
        ->get();

        $allUser = $this->get_all_user();
        // get viewtype from database
        $usermt = new usermeta;
        $viewtype = $usermt
        ->where('user_id', '=', 1)
        ->get();

        return Views::ResponseJson(
            'component.'.$viewtype['viewstyle'],
            [
                'head' => ['ชื่อ', 'วันที่สร้าง', 'ลบ'],
                'recs' => $allUser
            ]
        );
    }
    public function delete($params)
    {
        $post = RequestRequest::getInstants()->getAll();
        $user = new DatabaseUser;
        $user->delete()->where('id', '=', $post['id'])->get();

        echo json_encode(['error' => false]);
    }
    function get_all_user()
    {
        $user = new DatabaseUser;
        return $user->select('id', 'name', 'create_at')->get();
    }
    function add_userMeta($request)
    {
        $usermt = new usermeta;
        $usermt->insertOrupdate('user_id', $request['user_id'], [
            'meta_key' => 'viewstyle',
            'meta_value' => $request['viewstyle']
        ]);
    }
}