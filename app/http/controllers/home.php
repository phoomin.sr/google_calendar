<?php namespace controllers;

use app\database\user;
use app\database\usermeta;
use providers\view\Views;
use models\base;

class home {
    function index() 
    {
        $user = new user;
        $usermt = new usermeta;
        $allUser = $user->select('id', 'name', 'create_at')->get();
        $show = $usermt->where('user_id', '=', 1)->get();
        
        if (!isset($show['viewstyle'])) {
            $show['viewstyle'] = 'list';
        }
        
        return Views::render('home',
        array(
            'head' => ['ชื่อ', 'สร้างเมื่อ', 'ลบ'],
            'recs' => $allUser,
            'show' => $show
        ));
    }
}