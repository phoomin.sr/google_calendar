<?php namespace app\database;

class usermeta extends config{
    protected $table = 'usersmeta';
    protected $fillable = [
        'user_id', 'meta_key', 'meta_value'
    ];
    public $select = "meta_key, meta_value";
    public $fetchType = "keypair";
}