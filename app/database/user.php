<?php namespace app\database;


class user extends config {
    protected $table = "user";
    protected $fillable = [
        'name', 'password'
    ];
    public $timestamps = true;
}