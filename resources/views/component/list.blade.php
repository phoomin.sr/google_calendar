@for ($i = 0; $i < count($recs); $i++)
    <div class="card mb-2">
        <div class="card-header">
            {{ $recs[$i]['name'] }}
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <footer class="blockquote-footer">created at <cite title="Source Title">{{ $recs[$i]['create_at'] }}</cite></footer>
            </blockquote>
        </div>
    </div>
@endfor