<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="./vendor/components/font-awesome/css/all.min.css">

    <!-- google calendar -->
    <link rel="stylesheet" href="./public/calendar/lib/main.css">

    <!-- date range picker -->
    <link rel="stylesheet" href="./public/daterangepicker-master/daterangepicker.css">

    @yield('style')
    <title>home</title>
</head>
<body>
    @yield('contents')
    <!-- script -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <script src="./vendor/components/jquery/jquery.min.js"></script>

    <!-- google calendar -->
    <script src="./public/calendar/lib/main.js"></script>

    <!-- date range picker -->
    <script src="./public/daterangepicker-master/moment.min.js"></script>
    <script src="./public/daterangepicker-master/daterangepicker.js"></script>
    
    <script>
        $(document).ready(function () {
            $('form').submit(function (e) {
                e.preventDefault();
                var formId = $(this).attr('id');
                var formReq = $(this).attr('action');
                var form_data = replace_form_value($('form#'+formId).serializeArray());
                
                funcName = 'submit_' + formId;
                window[funcName](form_data, formReq);
            });
        });
        function replace_form_value(formvalue) {
            data = {};
            $.each(formvalue, function (key, value) {
                data[value.name] = value.value;
            });
            return data;
        }
        async function send(url = '', type = 'POST', data = {}) {
        const response = await fetch(url, {
            method: type,
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data)
        });
            return response.json();
        }
    </script>
    @yield('script')
</body>
</html>