<?php namespace providers\view;

use Exception;
use eftec\bladeone\BladeOne;

class Views {
    public static $path = "/resources/views";
    public static $castpath = "/storages/views";
    public static $globaldata = array();
    public static function render($view, $data) 
    {
        $blade = new BladeOne(dirname(__DIR__, 2) . Views::$path, dirname(__DIR__, 2) . Views::$castpath);
        return $blade->setView($view)
            ->share($data)
            ->run();
    }
    public static function ResponseJson($view, $data)
    {
        $blade = new BladeOne(dirname(__DIR__, 2) . Views::$path, dirname(__DIR__, 2) . Views::$castpath);
        return json_encode($blade->setView($view)
            ->share($data)
            ->run());
    }
}