<?php namespace providers\routes;

use providers\routes\Route;
use Phroute\Phroute\Route as RouteRoute;
use Phroute\Phroute\RouteCollector;

class routeController extends RouteCollector
{
    public function get($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::GET, $route, $this->handler($handler), $filters);
    }
    public function post($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::POST, $route, $this->handler($handler), $filters);
    }
    public function update($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::PUT, $route, $this->handler($handler), $filters);
    }
    public function delete($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::DELETE, $route, $this->handler($handler), $filters);
    }
    public function any($route, $handler, array $filters = [])
    {
        return $this->addRoute(RouteRoute::ANY, $route, $this->handler($handler), $filters);
    }
    public function handler($handler)
    {
        if (is_array($handler)&&count($handler) == 1)
        {
            array_push($handler, 'run_func_bymethod');
        }

        return $handler;
    }
    public function __destruct()
    {
        $Routes = new Route;
        $Routes->getOutput($this);
    }
}