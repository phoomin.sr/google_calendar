<?php namespace providers\request;

class Request {
    public $APPNAME = "";

    public function __construct()
    {
        $inputJSON = file_get_contents('php://input');
        $this->request = json_decode($inputJSON, true);
    }
    static function getInstants()
    {
        return new Request;
    }
    function get($name, $default = null)
    {
        if (isset($this->request[$name])) {
            $value = $this->request[$name];
        } else {
            $value = $default;
        }

        return $value;
    }
    function session()
    {
        $session = new session($this->APPNAME);
        return $session;
    }
    function getAll()
    {
        return $this->request;
    }
}