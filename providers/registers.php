<?php
require_once dirname(__DIR__) . "/vendor/autoload.php";

extract([
    "Routes" => new \providers\routes\Route,
    "tables" => new \providers\tables\tables,
    "request" => new \providers\request\Request,
    "router" => new \providers\routes\routeController(new Phroute\Phroute\RouteParser)
]);